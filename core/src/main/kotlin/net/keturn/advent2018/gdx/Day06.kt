package net.keturn.advent2018.gdx

import kotlin.math.abs
import kotlin.system.measureTimeMillis

enum class Direction {
    NORTH, EAST, SOUTH, WEST
}


data class Coordinate(val x: Int, val y: Int) {
    companion object {
        @Suppress("unused")
        fun fromString(s: String): Coordinate {
            val (x, y) = s.split(',').map { it.trim().toInt() }
            return Coordinate(x, y)
        }
    }

    fun distance(other: Coordinate): Int {
        return abs(this.x - other.x) + abs(this.y - other.y)
    }

    fun closest(others: Collection<Coordinate>): Coordinate? {
        val distances = others.map { it to this.distance(it) }
        val (_, minDistance) = distances.minBy { (_, distance) -> distance}!!
        return distances.singleOrNull { (_, distance) -> distance == minDistance }?.first
    }

    fun isBetween(cornerA: Coordinate, cornerB: Coordinate): Boolean {
        return (x.isBetween(cornerA.x, cornerB.x)) && (y.isBetween(cornerA.y, cornerB.y))
    }

    fun xMidwayTo(other: Coordinate): Coordinate {
        val dist = distance(other)
        val midDist = if (dist % 2 == 0) (dist / 2 - 1) else (dist / 2)
        val xDirection = (other.x - this.x).coerceIn(-1, 1)
        val yDirection = (other.y - this.y).coerceIn(-1, 1)

        return if (midDist <= abs(other.x - this.x)) {
            Coordinate(x + xDirection * midDist, this.y)
        } else {
            val spent = abs(other.x - this.x)
            Coordinate(other.x, this.y + yDirection * (midDist - spent))
        }
    }

    fun yMidwayTo(other: Coordinate): Coordinate {
        val dist = distance(other)
        val midDist = if (dist % 2 == 0) (dist / 2 - 1) else (dist / 2)
        val xDirection = (other.x - this.x).coerceIn(-1, 1)
        val yDirection = (other.y - this.y).coerceIn(-1, 1)

        return if (midDist <= abs(other.y - this.y)) {
            Coordinate(x, this.y + yDirection * midDist)
        } else {
            val spent = abs(other.y - this.y)
            Coordinate(x + xDirection * (midDist - spent), other.y)
        }
    }

    fun directionTo(other: Coordinate): Collection<Direction> {
        return directionalPredicates.mapNotNull { (f, direction) ->
            if (f(other, this)) direction else null
            // direction.takeIf { f(other, this) }
        }
    }

    fun isNorthOf(other: Coordinate): Boolean = (other.y > this.y) && (abs(this.x - other.x) <= (other.y - this.y))
    fun isSouthOf(other: Coordinate): Boolean = (this.y > other.y) && (abs(this.x - other.x) <= (this.y - other.y))
    fun isEastOf(other: Coordinate): Boolean = (this.x > other.x) && (abs(this.y - other.y) <= (this.x - other.x))
    fun isWestOf(other: Coordinate): Boolean = (other.x > this.x) && (abs(this.y - other.y) <= (other.x - this.x))

    private val directionalPredicates = listOf(
        Coordinate::isNorthOf to Direction.NORTH,
        Coordinate::isSouthOf to Direction.SOUTH,
        Coordinate::isEastOf to Direction.EAST,
        Coordinate::isWestOf to Direction.WEST
    )

}


fun inputCoords(): List<Coordinate> {
    return listOf(
        Coordinate(305, 349),
        Coordinate(315, 193),
        Coordinate(154, 62),
        Coordinate(246, 310),
        Coordinate(145, 283),
        Coordinate(260, 324),
        Coordinate(342, 79),
        Coordinate(321, 353),
        Coordinate(40, 242),
        Coordinate(351, 353),
        Coordinate(337, 297),
        Coordinate(174, 194),
        Coordinate(251, 160),
        Coordinate(314, 195),
        Coordinate(114, 81),
        Coordinate(204, 246),
        Coordinate(203, 169),
        Coordinate(203, 296),
        Coordinate(60, 276),
        Coordinate(201, 47),
        Coordinate(206, 96),
        Coordinate(243, 46),
        Coordinate(295, 304),
        Coordinate(319, 80),
        Coordinate(213, 330),
        Coordinate(337, 255),
        Coordinate(40, 262),
        Coordinate(302, 150),
        Coordinate(147, 349),
        Coordinate(317, 240),
        Coordinate(96, 315),
        Coordinate(133, 305),
        Coordinate(320, 348),
        Coordinate(210, 300),
        Coordinate(266, 216),
        Coordinate(223, 319),
        Coordinate(207, 152),
        Coordinate(127, 214),
        Coordinate(312, 245),
        Coordinate(49, 329),
        Coordinate(211, 84),
        Coordinate(129, 276),
        Coordinate(247, 143),
        Coordinate(208, 235),
        Coordinate(271, 126),
        Coordinate(124, 211),
        Coordinate(144, 184),
        Coordinate(54, 88),
        Coordinate(354, 300),
        Coordinate(148, 85)
    )
}


fun findAreas(coordinates: List<Coordinate>): Map<Coordinate, Int> {
    val closestMap = closestForAllPoints(coordinates).values

    return closestMap.filterNotNull().groupingBy { it }.eachCount()
//    val closestMap = everyplace.parallelStream().map { place -> place.closest(coordinates) }
//    val areaCounts = closestMap.filter(Objects::nonNull).collect(Collectors.groupingByConcurrent({it}, Collectors.counting())).toMap()
//    @Suppress("UNCHECKED_CAST")
//    return areaCounts as Map<Coordinate, Long>
}

fun closestForAllPoints(coordinates: List<Coordinate>): Map<Coordinate, Coordinate?> {
//    val minX = coordinates.minBy { it.x }!!.x
//    val maxX = coordinates.maxBy { it.x }!!.x
//    val minY = coordinates.minBy { it.y }!!.y
//    val maxY = coordinates.maxBy { it.y }!!.y

    val minX = 0
    val minY = 0
    val maxX = 500
    val maxY = 500

    val everyplace = (minX..maxX).flatMap { x -> (minY..maxY).map { y -> Coordinate(x, y) } }

    val closestMap = everyplace.associateWith { place -> place.closest(coordinates) }
    return closestMap
}


fun isInfinite(areaAround: Coordinate, allSites: List<Coordinate>): Boolean {
    val openNorth = allSites.none { blocker -> blocker.isNorthOf(areaAround) }
    val openSouth = allSites.none { blocker -> blocker.isSouthOf(areaAround) }
    val openEast = allSites.none { blocker -> blocker.isEastOf(areaAround) }
    val openWest = allSites.none { blocker -> blocker.isWestOf(areaAround) }
    return openNorth || openSouth || openEast || openWest
}


fun findNeighbors(coordinates: List<Coordinate>, place: Coordinate): List<Coordinate> {
    val sortedByDistance = coordinates.sortedBy { place.distance(it) }.drop(1)  // exclude self

    val neighbors = mutableListOf<Coordinate>()

    for (otherCoordinate in sortedByDistance) {
        val directionsFromHereToOther = place.directionTo(otherCoordinate)
        println("Considering ${otherCoordinate} ${directionsFromHereToOther} from ${place}")

        val maybeAdjacencyList = listOfNotNull(
            Direction.NORTH.takeIf { otherCoordinate.y < place.y },
            Direction.SOUTH.takeIf { otherCoordinate.y > place.y },
            Direction.WEST.takeIf { otherCoordinate.x < place.x },
            Direction.EAST.takeIf { otherCoordinate.x > place.x }
        )

        val neighborsInside = neighbors.filter { neigh -> neigh.isBetween(place, otherCoordinate) }
        val directionsBlockedByContainedNeighbor = neighborsInside.flatMap { place.directionTo(it) }

        if (directionsBlockedByContainedNeighbor.containsAll(maybeAdjacencyList)) {
            continue
        }

        val occupiedFaces = neighbors.flatMap {neigh ->
            place.directionTo(neigh)
        }

        if (occupiedFaces.containsAll(maybeAdjacencyList)) {
            continue
        }

        val blockedByNeighborsCone = neighbors.any { neigh ->
            val directionsFromHereToNeighbor = place.directionTo(neigh)
            val directionsFromNeighborToOther = neigh.directionTo(otherCoordinate)
            return@any directionsFromNeighborToOther.containsAny(directionsFromHereToNeighbor)
        }

        if (blockedByNeighborsCone) continue

        neighbors.add(otherCoordinate)
    }

    return neighbors.toList()
}


fun buildNeighborMap(coordinates: List<Coordinate>): Map<Coordinate, List<Coordinate>> {
    return coordinates.associateWith { findNeighbors(coordinates, it) }
}


fun main(args: Array<String>) {
    val inputs = inputCoords()

    println("${inputs.size} inputs")

    var areas: Map<Coordinate, Int>? = null
    val areaTime = measureTimeMillis {
        areas = findAreas(inputs)
    }
    println("found areas in time $areaTime ms")
    areas!!.forEach { coordinate, area -> println("$coordinate\t$area") }
}
