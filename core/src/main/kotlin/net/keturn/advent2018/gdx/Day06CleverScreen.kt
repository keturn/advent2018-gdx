package net.keturn.advent2018.gdx

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import ktx.app.KtxScreen
import ktx.app.clearScreen


fun ShapeRenderer.line(x: Int, y: Int, x2: Int, y2: Int) {
    return line(x.toFloat(), y.toFloat(), x2.toFloat(), y2.toFloat())
}


class Day06CleverScreen(val application: Advent2018) : KtxScreen {
    val inputs: List<Coordinate>
    val colorsForInputs: Map<Coordinate, Color>
    val neighborMap: Map<Coordinate, List<Coordinate>>
    private val shapeRenderer = ShapeRenderer()
    private var frameCounter = 0L

    init {
        shapeRenderer.scale(2f, 2f, 1f)
        Gdx.gl.glEnable(GL20.GL_BLEND)
        Gdx.gl.glBlendFunc(
            GL20.GL_SRC_ALPHA,
            GL20.GL_ONE_MINUS_SRC_ALPHA
        )
        inputs = inputCoords().sortedBy { it.x + it.y }
        colorsForInputs = inputs.zip(colorPalette.cycle().asIterable()).toMap()

        neighborMap = buildNeighborMap(inputs)
        neighborMap.forEach { place, neighbors -> println("${place} is near ${neighbors.size}: ${neighbors}") }
    }


    override fun render(delta: Float) {
        frameCounter++
        clearScreen(1f, 1f, 1f)
        with(shapeRenderer) {
            begin(ShapeRenderer.ShapeType.Filled)
            for ((place, neighbors) in neighborMap) {
                color = colorsForInputs[place]
                for (neighbor in neighbors) {
                    val xCorner = place.xMidwayTo(neighbor)
                    val yCorner = place.yMidwayTo(neighbor)
                    line(xCorner.x, xCorner.y, yCorner.x, yCorner.y)
//                    line(place.x, place.y, neighbor.x, place.y)
//                    line(place.x, place.y, place.x, neighbor.y)
                }
            }

//            val place = inputs[(frameCounter % inputs.size).toInt()]
//            color = colorsForInputs[place]
//            val neighbors = neighborMap.getValue(place)
//            for (neighbor in neighbors) {
//                line(place.x, place.y, neighbor.x, place.y)
//                line(place.x, place.y, place.x, neighbor.y)
//            }
            end()
        }
    }

    override fun dispose() {
        shapeRenderer.dispose()
    }
}
