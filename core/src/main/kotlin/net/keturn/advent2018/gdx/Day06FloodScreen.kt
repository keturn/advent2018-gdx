package net.keturn.advent2018.gdx

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.viewport.ExtendViewport
import ktx.actors.KtxInputListener
import ktx.actors.plus
import ktx.app.KtxScreen
import ktx.app.clearScreen
import ktx.assets.file
import kotlin.math.sin


class Day06FloodScreen(private val application: Advent2018) : KtxScreen {
    val inputs: List<Coordinate>
    val allPoints: Map<Coordinate, Coordinate>
    val colorsForInputs: Map<Coordinate, Color>
    private val shapeRenderer = ShapeRenderer()
    private var frameCounter = 0L

    private val viewport = ExtendViewport(400f, 450f)

    private var inputListener: KtxInputListener

    private val stage = Stage(viewport)
    private val distanceLabels: Map<Coordinate, Label>

    var selectedPoint: Coordinate? = null

    init {
        inputs = inputCoords().sortedBy { it.x + it.y }
        allPoints = closestForAllPoints(inputs).filterNotNullValues()
        colorsForInputs = inputs.zip(colorPalette.cycle().asIterable()).toMap()

        val skinFile = file("ui").child("skin.json")
        val skin = Skin(skinFile)

        inputListener = object : KtxInputListener() {
            override fun touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean {
                this@Day06FloodScreen.selectPoint(x, y)
                return true
            }

            override fun keyTyped(event: InputEvent, character: Char): Boolean {
                return application.handleKey(character)
            }
        }
        stage.addListener(inputListener)

        val rootTable = Table()
        rootTable.setFillParent(true)
        stage + rootTable

        distanceLabels = inputs.associateWith { Label("", skin) }
        distanceLabels.forEach { coord, label ->
            label.setFontScale(0.5f)
            label.x = coord.x.toFloat()
            label.y = coord.y.toFloat()
            stage + label
        }
    }

    private fun selectPoint(x: Float, y: Float) {
        selectedPoint = Coordinate(x.toInt(), y.toInt())
        recalcLabels()
    }

    private fun recalcLabels() {
        selectedPoint?.let {
            for ((coord, label) in distanceLabels) {
                label.setText(coord.distance(it).toString())
            }
        }
    }

    override fun show() {
        Gdx.input.inputProcessor = stage
        Gdx.gl.glEnable(GL20.GL_BLEND)
        Gdx.gl.glBlendFunc(
            GL20.GL_SRC_ALPHA,
            GL20.GL_ONE_MINUS_SRC_ALPHA
        )
    }

    override fun resize(width: Int, height: Int) {
        viewport.update(width, height, true)
        shapeRenderer.projectionMatrix = viewport.camera.combined
    }

    override fun render(delta: Float) {
        frameCounter++
        clearScreen(1f, 1f, 1f)
        Gdx.gl.glEnable(GL20.GL_BLEND)
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
        for ((spot, nearest) in allPoints) {
            val color = colorsForInputs[nearest]!!
            if (isInfinite(nearest, inputs)) {
                val alpha = (sin(frameCounter.toFloat() / 32f + 3 * color.g) + 1) / 4f + 0.1f
                shapeRenderer.setColor(color.r, color.g, color.b, alpha)
            } else {
                shapeRenderer.color = color
            }
            shapeRenderer.point(spot.x.toFloat(), spot.y.toFloat(), 0f)
        }
        for (spot in inputs) {
            shapeRenderer.color = colorPalette.random()
            shapeRenderer.point(spot.x.toFloat(), spot.y.toFloat(), 0f)
        }
        selectedPoint?.let {
            shapeRenderer.color = Color.BLACK
            shapeRenderer.point(it.x.toFloat(), it.y.toFloat(), 0f)
        }
        shapeRenderer.end()
        stage.draw()
//        screenshot()
    }


    override fun dispose() {
        shapeRenderer.dispose()
    }
}