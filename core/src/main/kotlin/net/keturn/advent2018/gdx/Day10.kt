package net.keturn.advent2018.gdx

import com.badlogic.gdx.math.Vector2
import ktx.assets.file

data class MovingPoint(val position: Vector2, val velocity: Vector2)

val STARFIELD_X = (-50_000 .. 51_000)
val STARFIELD_Y = (-50_000 .. 51_000)

fun inputStars(): List<MovingPoint> {
    // example: position=< 20247,  40241> velocity=<-2, -4>
    val pattern = Regex("""position=<\s*(-?\d+),\s*(-?\d+)>\s+velocity=<\s*(-?\d+), \s*(-?\d+)>""")
    return file("inputs").child("10").reader().useLines {lines ->
        lines.map {line ->
            val numbers = pattern.find(line)!!.groupValues.drop(1).map { it.toInt() }
            MovingPoint(Vector2(numbers[0].toFloat(), numbers[1].toFloat()),
                Vector2(numbers[2].toFloat(), numbers[3].toFloat()))
        }.toList()
    }

//    return (0..100).map { MovingPoint(Vector2(it.toFloat() * 10, it.toFloat() * 10),Vector2(0f, 0f)) }.toList()
}


