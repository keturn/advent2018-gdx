package net.keturn.advent2018.gdx

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.viewport.ExtendViewport
import ktx.app.KtxScreen
import ktx.app.clearScreen
import ktx.math.plus
import ktx.math.times


class Day10Screen(val application: Advent2018) : KtxScreen {
    private val shapeRenderer = ShapeRenderer()
    private var frameCounter = 0L

    private var inputs: List<MovingPoint> = inputStars()

    private val viewport = ExtendViewport(400f, 200f) // FitViewport(STARFIELD_X.count().toFloat(), STARFIELD_Y.count().toFloat())

    init {
        Gdx.gl.glEnable(GL20.GL_BLEND)
        Gdx.gl.glBlendFunc(
            GL20.GL_SRC_ALPHA,
            GL20.GL_ONE_MINUS_SRC_ALPHA
        )
        shapeRenderer.color = Color.WHITE
    }

    override fun resize(width: Int, height: Int) {
        viewport.update(width, height)
        shapeRenderer.projectionMatrix = viewport.camera.combined
//        shapeRenderer.scale(-1f, -1f, 1f)
        shapeRenderer.translate(-100f, -50f, 0f)
    }

    override fun render(delta: Float) {
        val targetTime = 10_027f
        frameCounter++
        clearScreen(0.1f, 0.1f, 0.2f)
        shapeRenderer.begin(ShapeRenderer.ShapeType.Point)
        for (spot in inputs) {
            shapeRenderer.point(spot.position + (spot.velocity * targetTime))
        }
        shapeRenderer.end()
//        screenshot()
    }

    override fun dispose() {
        shapeRenderer.dispose()
    }
}


private fun ShapeRenderer.point(position: Vector2) {
    return point(position.x, position.y, 0f)
}
