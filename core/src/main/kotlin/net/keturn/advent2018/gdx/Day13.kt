package net.keturn.advent2018.gdx

import ktx.assets.file

enum class TrackTypes {
    NORTH_SOUTH,
    EAST_WEST,
    CURVE_EAST_NORTH,
    CURVE_EAST_SOUTH,
    CURVE_WEST_NORTH,
    CURVE_WEST_SOUTH,
    INTERSECTION
}


enum class CartDirections {
    NORTH,
    SOUTH,
    EAST,
    WEST;

    operator fun plus(track: TrackTypes): CartDirections {
        return when {
            track == TrackTypes.INTERSECTION -> error("INTERSECTION")
            track == TrackTypes.NORTH_SOUTH -> this
            track == TrackTypes.EAST_WEST -> this
            track == TrackTypes.CURVE_EAST_NORTH && this == WEST -> NORTH
            track == TrackTypes.CURVE_EAST_NORTH && this == SOUTH -> EAST
            track == TrackTypes.CURVE_EAST_SOUTH && this == WEST -> SOUTH
            track == TrackTypes.CURVE_EAST_SOUTH && this == NORTH -> EAST
            track == TrackTypes.CURVE_WEST_NORTH && this == EAST -> NORTH
            track == TrackTypes.CURVE_WEST_NORTH && this == SOUTH -> WEST
            track == TrackTypes.CURVE_WEST_SOUTH && this == EAST -> SOUTH
            track == TrackTypes.CURVE_WEST_SOUTH && this == NORTH -> WEST
            else -> error("$this + $track == ???")
        }
    }

    operator fun plus(turn: TurnDirections): CartDirections {
        return when {
            turn == TurnDirections.STRAIGHT -> this
            this == NORTH && turn == TurnDirections.LEFT -> WEST
            this == NORTH && turn == TurnDirections.RIGHT -> EAST
            this == SOUTH && turn == TurnDirections.LEFT -> EAST
            this == SOUTH && turn == TurnDirections.RIGHT -> WEST
            this == EAST && turn == TurnDirections.LEFT -> NORTH
            this == EAST && turn == TurnDirections.RIGHT -> SOUTH
            this == WEST && turn == TurnDirections.LEFT -> SOUTH
            this == WEST && turn == TurnDirections.RIGHT -> NORTH
            else -> error("$this + $turn == ???")
        }
    }
}


enum class TurnDirections {
    LEFT,
    STRAIGHT,
    RIGHT
}


val turnSequence = listOf(
    TurnDirections.LEFT,
    TurnDirections.STRAIGHT,
    TurnDirections.RIGHT
)


typealias CartCoordinate = Pair<Int, Int>


class CartSimulator(val trackMap: TrackMap, val carts: List<Cart>) {
    var time = 0

    fun tick(): List<CartCoordinate> {
        val collisions = mutableListOf<CartCoordinate>()
        time++
        for (cart in sortedCarts()) {
            if (cart.collided) {
                continue
            }
            val newPosition = cart.nextPosition()
            val nextTrack = trackAt(newPosition)
            val collidingCart = cartAt(newPosition)
            cart.move(nextTrack)
            if (collidingCart != null) {
                cart.collided = true
                collidingCart.collided = true
                collisions.add(cart.position)
            }
        }
        if (collisions.isNotEmpty()) {
            val nonCollided = carts.singleOrNull { !it.collided }
            if (nonCollided != null) {
                println("LAST CART at ${nonCollided.position.second},${nonCollided.position.first}")
            }
        }
        return collisions.toList()
    }

    fun sortedCarts(): List<Cart> {
        return carts.sortedWith(compareBy({ it.position.first }, { it.position.second }))
    }

    fun cartAt(position: CartCoordinate, includeCollided: Boolean = false): Cart? {
       return carts.firstOrNull() { (includeCollided || !it.collided) && it.position == position }
    }

    fun trackAt(position: CartCoordinate): TrackTypes {
        val track = trackMap.getOrNull(position.first)?.getOrNull(position.second)
        return if (track != null) track else error("off the track! ${position}")
    }
}


class Cart(var position: CartCoordinate, var heading: CartDirections) {
    constructor(row: Int, column: Int, heading: CartDirections) : this(CartCoordinate(row, column), heading)

    var turnCounter = 0
    var collided = false

    fun nextPosition(): CartCoordinate {
        return when (heading) {
            CartDirections.NORTH -> position.toNorth
            CartDirections.SOUTH -> position.toSouth
            CartDirections.EAST-> position.toEast
            CartDirections.WEST -> position.toWest
        }
    }

    fun move(track: TrackTypes): CartCoordinate {
        position = nextPosition()
        val newDirection = when (track) {
            TrackTypes.INTERSECTION -> {
                val turn = turnSequence[turnCounter % turnSequence.size]
                turnCounter++
                heading + turn
            }
            else -> heading + track
        }
        heading = newDirection
        return position
    }
}

private val CartCoordinate.toNorth: CartCoordinate
    get() = CartCoordinate(first - 1, second)
private val CartCoordinate.toSouth: CartCoordinate
    get() = CartCoordinate(first + 1, second)
private val CartCoordinate.toEast: CartCoordinate
    get() = CartCoordinate(first, second + 1)
private val CartCoordinate.toWest: CartCoordinate
    get() = CartCoordinate(first, second - 1)


fun parseTrackLine(rowNumber: Int, line: String): Pair<List<TrackTypes?>, List<Cart>> {
    var prev = ' '
    val carts = mutableListOf<Cart>()
    val track = mutableListOf<TrackTypes?>()
    for ((pos, c) in line.withIndex()) {
        val thisTrack = when {
            '-' == c -> TrackTypes.EAST_WEST
            '|' == c -> TrackTypes.NORTH_SOUTH
            '+' == c -> TrackTypes.INTERSECTION
            '/' == c && (prev in "-+") -> TrackTypes.CURVE_WEST_NORTH
            '/' == c -> TrackTypes.CURVE_EAST_SOUTH
            '\\' == c && (prev in "-+") -> TrackTypes.CURVE_WEST_SOUTH
            '\\' == c -> TrackTypes.CURVE_EAST_NORTH
            // Carts:
            '>' == c -> TrackTypes.EAST_WEST
            '<' == c -> TrackTypes.EAST_WEST
            'v' == c -> TrackTypes.NORTH_SOUTH
            '^' == c -> TrackTypes.NORTH_SOUTH
            ' ' == c -> null
            else -> error("unknown track specifier \"$c\"")
        }
        track.add(thisTrack)

        val thisCart = when (c) {
            '>' -> Cart(rowNumber, pos, CartDirections.EAST)
            '<' -> Cart(rowNumber, pos, CartDirections.WEST)
            'v' -> Cart(rowNumber, pos, CartDirections.SOUTH)
            '^' -> Cart(rowNumber, pos, CartDirections.NORTH)
            else -> null
        }
        if (thisCart != null) {
            carts.add(thisCart)
        }
        prev = c
    }
    return Pair(track.toList(), carts.toList())
}


typealias TrackMap = List<List<TrackTypes?>>


fun readTrackFile(): Pair<TrackMap, List<Cart>> {
    val parseResults = file("inputs").child("13").reader().useLines { lines ->
        lines.mapIndexed(::parseTrackLine).toList()
    }

    val trackMap = parseResults.map { it.first }
    val carts = parseResults.flatMap { it.second }

    return Pair(trackMap, carts)
}
