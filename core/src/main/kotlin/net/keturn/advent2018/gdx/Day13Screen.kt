package net.keturn.advent2018.gdx

import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.viewport.ExtendViewport
import ktx.app.KtxScreen
import ktx.app.clearScreen
import ktx.graphics.triangle

// thanks again Colorgorical http://vrl.cs.brown.edu/color
val cartColors = listOf(
    color(114,229,239),
    color(204,21,109),
    color(141,251,126),
    color(239,102,240),
    color(110,174,61),
    color(198,151,244),
    color(206,238,148),
    color(132,83,182),
    color(249,248,53),
    color(58,120,195),
    color(231,134,7),
    color(72,159,144),
    color(248,56,27),
    color(202,211,250),
    color(135,99,65),
    color(250,184,153),
    color(166,77,108)
)

class Day13Screen(val application: Advent2018) : KtxScreen {
    private val shapes = ShapeRenderer()
    private var frameCounter = 0L

    private val viewport = ExtendViewport(152f, 150f) // FitViewport(STARFIELD_X.count().toFloat(), STARFIELD_Y.count().toFloat())

    private val sim: CartSimulator

    init {
        val (trackMap, carts) = readTrackFile()
        sim = CartSimulator(trackMap, carts)

        println("${trackMap.size} rows of track, ${carts.size} carts")

    }

    override fun resize(width: Int, height: Int) {
        viewport.update(width, height, true)
        shapes.projectionMatrix = viewport.camera.combined
    }

    override fun render(delta: Float) {
        frameCounter++

        val collisions = sim.tick()
        if (collisions.isNotEmpty()) {
            println("*** COLLISION at tick ${sim.time} ***")
            collisions.forEach { println("${it.second},${it.first}") }
        }

        clearScreen(0.0f, 0.0f, 0.0f)
        shapes.begin(ShapeRenderer.ShapeType.Line)
        shapes.setColor(0.7f, 0.4f, 0.2f, 1f)
        val topY = sim.trackMap.size.toFloat()
        for((rowInt, trackRow) in sim.trackMap.withIndex()) {
            val rowY = topY - rowInt.toFloat()
            for ((colInt, track) in trackRow.withIndex()) {
                val colX = colInt.toFloat()
                val here = Vector2(colX, rowY)
                if (track == null) {
                    continue
                }
                when (track) {
                    TrackTypes.NORTH_SOUTH -> shapes.line(here.north(), here.south())
                    TrackTypes.EAST_WEST -> shapes.line(here.east(), here.west())
                    TrackTypes.INTERSECTION -> {
                        shapes.line(here.north(), here.south())
                        shapes.line(here.east(), here.west())
                    }
                    TrackTypes.CURVE_EAST_NORTH ->
                        shapes.line(here.east(), here.north())
                    TrackTypes.CURVE_WEST_SOUTH ->
                        shapes.line(here.west(), here.south())
                    TrackTypes.CURVE_EAST_SOUTH ->
                        shapes.line(here.east(), here.south())
                    TrackTypes.CURVE_WEST_NORTH ->
                        shapes.line(here.west(), here.north())
                }
            }
        }
        shapes.end()
        shapes.begin(ShapeRenderer.ShapeType.Filled)
        for ((cartN, cart) in sim.carts.withIndex()) {
            val here = cart.position.toVector(topY)
            shapes.color = cartColors[cartN % colorPalette.size]
            when (cart.heading) {
                CartDirections.NORTH -> shapes.triangle(here.north(), here.sw(), here.se())
                CartDirections.EAST -> shapes.triangle(here.east(), here.nw(), here.sw())
                CartDirections.SOUTH -> shapes.triangle(here.south(), here.nw(), here.ne())
                CartDirections.WEST -> shapes.triangle(here.west(), here.ne(), here.se())
            }
            if (cart.collided) {
                shapes.x(here.center(), 1f)
            }
        }
        shapes.end()
//        screenshot()
    }

    override fun dispose() {
        shapes.dispose()
    }
}

private fun Vector2.north(): Vector2 = Vector2(x + 0.5f, y + 1f)
private fun Vector2.south(): Vector2 = Vector2(x + 0.5f, y)
private fun Vector2.east(): Vector2 = Vector2(x + 1f, y + 0.5f)
private fun Vector2.west(): Vector2 = Vector2(x, y + 0.5f)

private fun Vector2.nw(): Vector2 = Vector2(x, y + 1f)
private fun Vector2.ne(): Vector2 = Vector2(x + 1f, y + 1f)
private fun Vector2.sw(): Vector2 = this
private fun Vector2.se(): Vector2 = Vector2(x + 1f, y)

internal fun Vector2.center(): Vector2 = Vector2(x + 0.5f, y + 0.5f)

private fun CartCoordinate.toVector(topY: Float): Vector2 {
    return Vector2(this.second.toFloat(), topY - this.first.toFloat())
}
