package net.keturn.advent2018.gdx

import ktx.assets.file

const val WALL_CHAR = '#'

const val INITIAL_HP = 200
const val ATTACK_POWER = 3
const val ELF_ATTACK_POWER = 19


class Terrain(private val walls: List<BooleanArray>): List<BooleanArray> by walls {
    constructor(xSize: Int, ySize: Int) : this(List(xSize) { BooleanArray(ySize) })

    val xSize = walls.size
    val ySize = walls[0].size

    fun iterateWalls(): Sequence<CombatCoordinate> {
        return sequence {
            for ((x, column) in walls.withIndex()) {
                for ((y, isWall) in column.withIndex()) {
                    if (isWall) {
                        yield(CombatCoordinate(x, y))
                    }
                }
            }
        }
    }

    fun copy(): Terrain {
        val newThing = Terrain(xSize, ySize)

        for ((x, column) in walls.withIndex()) {
            column.copyInto(newThing[x])
        }

        return newThing
    }

    fun block(position: CombatCoordinate) {
        this[position.x][position.y] = true
    }

    fun clear(position: CombatCoordinate) {
        this[position.x][position.y] = false
    }

    fun isBlockedAt(position: CombatCoordinate): Boolean {
        return this[position.x][position.y]
    }

    fun isBlockedAt(x: Int, y:Int): Boolean {
        return this[x][y]
    }
}


enum class CombatFaction(private val c: Char) {
    ELVES('E'),
    GOBLINS('G');

    fun equals(other: Char): Boolean {
        return c == other
    }
}


/*
 * Another Coordinate class?
 *
 * Yeah I probably could consolidate these integer-based 2D Coordinate classes.
 */
data class CombatCoordinate(val x: Int, val y: Int) {
    fun adjacentPositions(): Sequence<CombatCoordinate> {
        return sequence {
            yield(CombatCoordinate(x, y - 1))
            yield(CombatCoordinate(x - 1, y))
            yield(CombatCoordinate(x + 1, y))
            yield(CombatCoordinate(x, y + 1))
        }
    }
}


class Mob(val faction: CombatFaction, var position: CombatCoordinate) {
    var hp = INITIAL_HP

    fun move(newPosition: CombatCoordinate) {
        position = newPosition
    }

    fun damage(amount: Int): Boolean {
        hp -= amount
        return !living
    }

    fun attackPower(): Int {
        return when (faction) {
            CombatFaction.ELVES -> ELF_ATTACK_POWER
            CombatFaction.GOBLINS -> ATTACK_POWER
        }
    }

    val living: Boolean get() = hp > 0

    override fun toString(): String {
        return "$faction at [${position.x},${position.y}]"
    }
}


private class DistanceMap(private val array: List<Array<Short?>>) {
    constructor(xSize: Int, ySize: Int) : this(List(xSize) { Array<Short?>(ySize) {null} })

    fun recompute(origin: CombatCoordinate, terrain: Terrain) {
        val xSize = terrain.xSize
        val ySize = terrain.ySize

        blank()
        origin.adjacentPositions().forEachIndexed { i, here ->
            if (!terrain.isBlockedAt(here)) {
                array[here.x][here.y] = 1
            }
        }
        var stable: Boolean
        do {
            stable = true
            for (x in (1 until xSize)) {
                for (y in (1 until ySize)) {
                    if ((array[x][y] == null) and !terrain.isBlockedAt(x, y)) {
                        val here = CombatCoordinate(x, y)
                        val nearValues = here.adjacentPositions().mapNotNull {
                            (nearX, nearY) -> array[nearX][nearY]
                        }.min()
                        if (nearValues != null) {
                            array[x][y] = (nearValues + 1).toShort()
                            stable = false
                        }
                    }
                }
            }
        } while (!stable)
    }

    private fun blank() {
        array.forEach { it.fill(null) }
    }

    fun nearestTo(position: CombatCoordinate): Pair<Short, CombatCoordinate>? {
        return position.adjacentPositions().mapNotNull { there ->
            val score = array[there.x][there.y]
            if (score != null) {
                return@mapNotNull score to there
            } else {
                return@mapNotNull null
            }
        }.minBy { it.first }
    }
}


class CombatSimulator(terrain: Terrain, initialMobLocations: List<Pair<CombatFaction, CombatCoordinate>>) {
    var time = 0
    var finalCompleteRound: Int? = null
    val mobs = mutableListOf<Mob>()
    private val terrainWithMobs: Terrain = terrain.copy()

    private val distanceMaps = mutableMapOf<Mob, DistanceMap>()

    init {
        for ((faction, position) in initialMobLocations) {
            val mob = Mob(faction, position)
            mobs.add(mob)
            distanceMaps[mob] = DistanceMap(terrain.xSize, terrain.ySize)
            terrainWithMobs.block(position)
        }

        // and now that terrainWithMobs is fully initialized
        recomputeDistanceMaps()
    }

    internal fun recomputeDistanceMaps() {
        for ((mob, dMap) in distanceMaps) {
            if (mob.living) {
                dMap.recompute(mob.position, terrainWithMobs)
            }
        }
    }

    fun tick(): Boolean {
        time++
        var combatOver = false
        nextMob@ for (mob in mobs.living().sortedByPosition()) {
            if (!mob.living) {
                continue
            }
            var dirty = false
            var adjacentTarget = adjacentEnemy(mob)
            if (adjacentTarget == null) {
                // move
                val nextStep = findNearestEnemy(mob)
                if (nextStep != null) {
                    terrainWithMobs.clear(mob.position)
                    mob.move(nextStep)
                    terrainWithMobs.block(mob.position)
                    dirty = true
                } else {
                    if (mobs.living().enemyOf(mob.faction).isEmpty()) {
                        if (finalCompleteRound == null) {
                            println("No enemies to pursue! Combat declared over during $time")
                            finalCompleteRound = time - 1  // we're mid-round; the prev one was complete
                            printScores()
                        }
                        combatOver = true
                        break@nextMob
                    }
                }

                adjacentTarget = adjacentEnemy(mob)
                // re-assess adjacent target
            }
            if (adjacentTarget != null) {
                val kill = adjacentTarget.damage(mob.attackPower())
                if (kill) {
                    dirty = true
                    terrainWithMobs.clear(adjacentTarget.position)
                    println("$time\t$mob killed $adjacentTarget!")
                }
            }
            if (dirty) {
                recomputeDistanceMaps()
            }
        }
        return combatOver
    }

    fun printScores() {
        val totesHP = mobs.living().groupingBy { it.faction }.fold(0) { total, it-> total + it.hp }
        println("Elf deaths: ${mobs.dead().filter { it.faction == CombatFaction.ELVES}.count()}")
        println("Total HP: $totesHP")
        finalCompleteRound?.let {round ->
            println("Final round was $round")
            println("Part One Checksum: ${totesHP.values.single() * round}")
        }
    }

    fun runToCompletion(): Pair<Int, Int> {
        var combatOver: Boolean
        do {
            combatOver = tick()
        } while (!combatOver)

        val totesHP = mobs.living().byFaction().fold(0) { total, it-> total + it.hp }
        return Pair(finalCompleteRound!!, totesHP.values.single())
    }

    private fun adjacentEnemy(mob: Mob): Mob? {
        val adjacentEnemies = mob.position.adjacentPositions().mapNotNull { there ->
            mobAt(there)?.takeIf { it.living && it.faction != mob.faction }
        }
        return adjacentEnemies.minWith(compareBy({ it.hp }, {it.position.y}, {it.position.x}))
    }

    private fun findNearestEnemy(mob: Mob): CombatCoordinate? {
        val enemies = mobs.living().enemyOf(mob.faction)

        var lowestDistance = Short.MAX_VALUE
        val mobsAtLowestDistance = mutableListOf<Pair<Mob, CombatCoordinate>>()

        for (enemy in enemies) {
            val dMap = distanceMaps[enemy]!!
            val (distanceToEnemy, firstStep) = dMap.nearestTo(mob.position) ?: continue
            if (distanceToEnemy < lowestDistance) {
                lowestDistance = distanceToEnemy
                mobsAtLowestDistance.clear()
            }
            if (distanceToEnemy == lowestDistance) {
                mobsAtLowestDistance.add(Pair(enemy, firstStep))
            }
        }

        if (mobsAtLowestDistance.isEmpty()) {
            return null  // no reachable mobs
        }

        val nextStep: CombatCoordinate
        nextStep = if (mobsAtLowestDistance.size == 1) {
            mobsAtLowestDistance[0].second
        } else {
            val dMap = distanceMaps[mob]!!
            val enemyAdjacentSpots = mobsAtLowestDistance.map { (enemy, stepTo) ->
                Pair(dMap.nearestTo(enemy.position)!!.second, stepTo)
            }
            val preferredTarget = enemyAdjacentSpots.minWith(compareBy({ it.first.y }, { it.first.x }))!!
            preferredTarget.second
        }

        return nextStep
    }

    fun mobAt(position: CombatCoordinate): Mob? {
        return mobs.living().singleOrNull { it.position == position }
    }
}


private fun Collection<Mob>.enemyOf(faction: CombatFaction): Collection<Mob> = filter { it.faction != faction }

internal fun Collection<Mob>.living(): Collection<Mob> = filter { it.living }
internal fun Collection<Mob>.dead(): Collection<Mob> = filter { !it.living }
internal fun Collection<Mob>.byFaction(): Grouping<Mob, CombatFaction> = groupingBy { it.faction }

private fun Collection<Mob>.sortedByPosition(): List<Mob> = sortedWith(compareBy({ it.position.y }, { it.position.x }))


fun inputCombatMap(): Pair<Terrain, List<Pair<CombatFaction, CombatCoordinate>>> {
    val inLines = file("inputs").child("15").reader().readLines()
    return  combatMapFromString(inLines)
}


internal fun combatMapFromString(s: String): Pair<Terrain, MutableList<Pair<CombatFaction, CombatCoordinate>>> {
    return combatMapFromString(s.lines())
}


internal fun combatMapFromString(inLines: List<String>): Pair<Terrain, MutableList<Pair<CombatFaction, CombatCoordinate>>> {
    val ySize = inLines.size
    val xSize = inLines.map { it.length }.max()!!

    val terrain = Terrain(xSize, ySize)
    val mobs = mutableListOf<Pair<CombatFaction, CombatCoordinate>>()
    for ((y, row) in inLines.withIndex()) {
        for ((x, c) in row.withIndex()) {
            terrain[x][y] = c == WALL_CHAR
            for (faction in CombatFaction.values()) {
                if (faction.equals(c)) {
                    mobs.add(Pair(faction, CombatCoordinate(x, y)))
                }
            }
        }
    }
    return Pair(terrain, mobs)
}
