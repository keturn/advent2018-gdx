package net.keturn.advent2018.gdx

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.viewport.ExtendViewport
import ktx.app.KtxScreen
import ktx.app.clearScreen
import ktx.graphics.circle
import ktx.graphics.rect


private const val MINIMUM_TICK_TIME = 0

val factionColors = mapOf(
    CombatFaction.ELVES to Pair(Color.CYAN, Color.BLUE),
    CombatFaction.GOBLINS to Pair(Color.GOLDENROD, Color.BROWN)
)


class Day15Screen(@Suppress("unused") val application: Advent2018) : KtxScreen {

    private val shapes = ShapeRenderer()
    private val viewport: ExtendViewport

    private val terrain: Terrain
    private val sim: CombatSimulator

    private val topY: Float

    private var combatOver = false

    private var lastTickTime = 0L

    init {
        val (initTerrain, mobs) = inputCombatMap()
        terrain = initTerrain

        viewport = ExtendViewport(terrain.xSize.toFloat(), terrain.ySize.toFloat())
        topY = terrain.ySize.toFloat()

        sim = CombatSimulator(terrain, mobs)
    }

    override fun resize(width: Int, height: Int) {
        viewport.update(width, height, true)
        shapes.projectionMatrix = viewport.camera.combined
    }

    override fun render(delta: Float) {
        val currentTime = System.currentTimeMillis()
//        val wasCombatOver = combatOver
        if (!combatOver && (currentTime - lastTickTime) > MINIMUM_TICK_TIME) {
            combatOver = sim.tick()
            lastTickTime = currentTime
        }
        clearScreen(0.0f, 0.0f, 0.0f)
        renderTerrain()
        renderMobs()
//        if (!wasCombatOver) {
//            screenshot("day15-wrong", sim.time.toLong())
//        }
    }

    private fun renderTerrain() {
        shapes.color = Color.DARK_GRAY
        shapes.begin(ShapeRenderer.ShapeType.Filled)
        for (loc in terrain.iterateWalls()) {
            val here = coordinateToVector(loc)
            shapes.rect(here, 1f, 1f)
        }
        shapes.end()
    }

    private fun renderMobs() {
        shapes.begin(ShapeRenderer.ShapeType.Filled)
        for (mob in sim.mobs.dead()) {
            val here = coordinateToVector(mob.position)
            shapes.color = mob.color
            shapes.circle(here.center(), 0.5f, 8)
        }
        for (mob in sim.mobs.living()) {
            val here = coordinateToVector(mob.position)
            shapes.color = mob.color
            shapes.circle(here.center(), 0.5f, 8)

            val hpX = here.x + mob.hp.toFloat() / INITIAL_HP
            val barY = here.y + 0.125f

            shapes.color = Color.NAVY
            shapes.rectLine(
                hpX, barY,
                here.x + 1, barY,
                0.125f
            )
            shapes.color = Color.GREEN
            shapes.rectLine(
                here.x, barY,
                hpX, barY,
                0.125f
            )


        }
        shapes.end()
    }

    private fun coordinateToVector(coord: CombatCoordinate): Vector2 {
        return Vector2(coord.x.toFloat(), topY - coord.y.toFloat() - 1)
    }
}


private val Mob.color: Color
    get() {
        val colors = factionColors[faction]!!
        return if (living) colors.first else colors.second
    }
