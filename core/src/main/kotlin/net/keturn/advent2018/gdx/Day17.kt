package net.keturn.advent2018.gdx

import ktx.assets.file
import java.util.Stack


const val SPRING_X = 500
const val SPRING_Y = 0

const val TOO_MANY = 1_000_000


enum class WaterTiles {
    SAND,
    CLAY,
    WATER_STILL,
    WATER_DRAINED
}


typealias WaterScanArray = List<Array<WaterTiles>>


@Suppress("FunctionName")
fun WaterScanArray(width: Int, height: Int): WaterScanArray {
    return List(width) { Array<WaterTiles>(height) { WaterTiles.SAND } }
}


private fun WaterScanArray.fill(value: WaterTiles, xRange: IntRange, yRange: IntRange) {
    for (x in xRange) {
        for (y in yRange) {
            this[x][y] = value
        }
    }
}

private operator fun WaterScanArray.get(x: Int, y: Int): WaterTiles {
    return this[x][y]
}


internal operator fun WaterScanArray.set(x: Int, y: Int, value: WaterTiles) {
    this[x][y] = value
}


class Spout private constructor(val x: Int, val y: Int, val parent: Spout? = null) {

    companion object {
        fun root(x: Int, y: Int): Spout {
            return Spout(x, y, null)
        }
    }

    fun child(x: Int, y: Int): Spout {
        return Spout(x, y, this)
    }

    operator fun component1() = x
    operator fun component2() = y
}


class WaterMap(internal val array: WaterScanArray, springX: Int = SPRING_X): WaterScanArray by array {
    val spouts = Stack<Spout>()
    var time = 0
    val maxY = array[0].lastIndex

    val xSize = array.size
    val ySize = array[0].size

    companion object {
        operator fun invoke(clayRanges: ClayRangeList): WaterMap {
            val (xRange, yRange) = clayRanges.bounds()
            println("x: $xRange, y: $yRange")
            val array = WaterScanArray(xRange.endInclusive + 2, yRange.endInclusive + 1)

            clayRanges.forEach { (xRange, yRange) ->
                array.fill(WaterTiles.CLAY, xRange, yRange)
            }

            return WaterMap(array)
        }
    }

    init {
        spouts.push(Spout.root(springX, SPRING_Y))
    }

    fun tick(): Boolean {
        // The puzzle doesn't talk about time at all, but we want an incremental processes so we can animate it.
        time++
        if (spouts.isNotEmpty()) {
            propagate(spouts.pop())
        } else {
            println("COMPLETE at ${time}")
            println("${score()} water tiles from y=0")
        }
        return spouts.isNotEmpty()
    }

    fun score(): Pair<Int, Int> {
        val drainedTiles = this.map { col -> col.count { it == WaterTiles.WATER_DRAINED }}.sum()
        val stillTiles = this.map { col -> col.count { it == WaterTiles.WATER_STILL}}.sum()
        return Pair(drainedTiles, stillTiles)
    }

    fun runUntilCompletion() {
        whileNotTooMany {
            tick()
        }
    }

    private fun propagate(spout: Spout) {
        val below = this[spout.x, spout.y + 1]
        when (below) {
            WaterTiles.SAND -> fallFrom(spout)
            WaterTiles.WATER_DRAINED -> fallFrom(spout)
            WaterTiles.CLAY -> horizontalFlow(spout)
            WaterTiles.WATER_STILL -> horizontalFlow(spout)
        }
    }

    private fun fallFrom(spout: Spout) {
        val x = spout.x
        var y = spout.y
        var reachedDrain = false
        falling@ do {
            if (y == maxY) {  // hit bottom!
                reachedDrain = true
                break
            }
            val down = this[x, y + 1]
            when (down) {
                WaterTiles.WATER_DRAINED -> { reachedDrain = true; break@falling }
                WaterTiles.CLAY -> { spouts.add(spout.child(x, y)); break@falling }
                WaterTiles.WATER_STILL -> { spouts.add(spout.child(x, y)); break@falling }
                WaterTiles.SAND -> Unit
            }
            y++
        } while (true)
        if (reachedDrain) {
            array.fill(WaterTiles.WATER_DRAINED, x..x, spout.y..y)
            completeDrainedSpout(spout)
        }
    }

    private fun completeDrainedSpout(spout: Spout) {
        val parent = spout.parent ?: return
        array.fill(WaterTiles.WATER_DRAINED, ascendingRange(parent.x, spout.x), ascendingRange(parent.y, spout.y))

        if (spout.y == parent.y) {
            val lowX = seekEdge(WaterTiles.WATER_STILL, -1, parent.x, parent.y)
            val highX = seekEdge(WaterTiles.WATER_STILL, 1, parent.x, parent.y)
            if (lowX != highX) {
                array.fill(WaterTiles.WATER_DRAINED, lowX..highX, parent.y..parent.y)
            }
        }

        return completeDrainedSpout(parent)
    }

    private fun horizontalFlow(spout: Spout) {
        val blockedLeft = horizontalFlow(spout, -1)
        val blockedRight = horizontalFlow(spout, 1)

        array.fill(
            WaterTiles.WATER_STILL,
            (blockedLeft ?: spout.x)..(blockedRight ?: spout.x),
            spout.y..spout.y
        )

        if ((blockedLeft != null) && (blockedRight != null)) {
            if (spout.parent != null) {
                if (spout.y < spout.parent.y) {
                    spouts.push(spout.parent.child(spout.x, spout.y - 1))
                } else {
                    spouts.push(spout.parent)
                }
            }
        }
    }

    private fun horizontalFlow(spout: Spout, dx: Int): Int? {
        val y = spout.y
        val down = y + 1
        var x = spout.x

        do {
            val next = this[x + dx, y]

            when (next) {
                WaterTiles.SAND -> Unit
                WaterTiles.CLAY -> return x
                else -> error("horizontal flow ran in to $next")
            }

            x += dx

            val below = this[x, down]
            when (below) {
                WaterTiles.CLAY -> Unit
                WaterTiles.WATER_STILL -> Unit
                WaterTiles.SAND -> { spouts.push(spout.child(x, y)); return null }
                else -> error("horizontal flow is over $below")
            }
        } while (true)
    }

    private fun seekEdge(value: WaterTiles, dx: Int, x: Int, y: Int): Int {
        var edgeX = x
        while (this[edgeX + dx, y] == value) {
            edgeX += dx
        }
        return edgeX
    }
}


typealias ClayRangeList = List<Pair<IntRange, IntRange>>


internal fun ClayRangeList.bounds(): Pair<IntRange, IntRange> {
    val minX = this.minBy { it.first.start }!!.first.start
    val maxX = this.maxBy { it.first.endInclusive }!!.first.endInclusive
    val minY = this.minBy { it.second.start }!!.second.start
    val maxY = this.maxBy { it.second.endInclusive }!!.second.endInclusive
    return Pair(minX .. maxX, minY ..maxY)
}


fun inputClay(): ClayRangeList {
    return file("inputs").child("17").reader().useLines {
        inputClay(it)
    }
}


fun inputClay(lines: Sequence<String>): ClayRangeList {
    return lines.map { line: String ->
        var parts = line.split(", ")

        if (line.startsWith('y')) {
            parts = parts.reversed()
        }

        val (xRange, yRange) = parts.map { it.split('=')[1].toIntRange() }
        Pair(xRange, yRange)
    }.toList()
}


private fun String.toIntRange(): IntRange {
    return if (".." in this) {
        val (start, endInclusive) = this.split("..").map(String::toInt)
        IntRange(start, endInclusive)
    } else {
        val only = this.toInt()
        IntRange(only, only)
    }
}


inline fun whileNotTooMany(function: () -> Boolean) {
    var c = 0
    while (c++ < TOO_MANY) {
        if (!function()) {
            return
        }
    }
    println("Too many iterations")
}


fun ascendingRange(a: Int, b: Int): IntRange {
    return if (a < b) {
        a..b
    } else {
        b..a
    }
}