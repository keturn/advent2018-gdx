package net.keturn.advent2018.gdx

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.utils.viewport.FillViewport
import ktx.app.KtxScreen
import ktx.app.clearScreen

private const val MINIMUM_TICK_TIME = 0L

class Day17Screen(@Suppress("unused") val application: Advent2018) : KtxScreen {

    private val shapes = ShapeRenderer()
    private val viewport: FillViewport

    private val clayRanges: ClayRangeList = inputClay()
    private val waterMap = WaterMap(clayRanges)
    private val topY: Float

    private val camera: OrthographicCamera get() = viewport.camera as OrthographicCamera
    private var lastTickTime = 0L

    init {
        // going to flip X & Y because the data is much taller than it is wide
        topY = waterMap.xSize.toFloat() + 2
        val minY = clayRanges.bounds().first.start.toFloat() - 2
        viewport = FillViewport(waterMap.ySize.toFloat(), topY - minY)
    }

    override fun resize(width: Int, height: Int) {
        viewport.update(width, height, true)
        shapes.projectionMatrix = viewport.camera.combined
    }

    override fun render(delta: Float) {
        clearScreen(0.0f, 0.0f, 0.0f)

        val currentTime = System.currentTimeMillis()
        if ((currentTime - lastTickTime) > MINIMUM_TICK_TIME) {
            if (waterMap.spouts.isNotEmpty()) {
                val (x, y) = waterMap.spouts.peek()!!
                focusOn(y.toFloat(), x.toFloat())
            }
            waterMap.tick()
            lastTickTime = currentTime
        }

        renderWalls()
        renderWater()
    }

    private fun focusOn(x: Float, @Suppress("UNUSED_PARAMETER") y: Float) {
        camera.position.x = x
        camera.update()
        shapes.projectionMatrix = camera.combined
    }

    private fun renderWalls() {
        shapes.color = Color.MAROON
        shapes.begin(ShapeRenderer.ShapeType.Filled)
        for ((xRange, yRange) in clayRanges) {
            // going to flip X & Y because the data is much taller than it is wide
            shapes.rect(
                yRange.start.toFloat(), topY - xRange.endInclusive,
                yRange.count().toFloat(), xRange.count().toFloat())
        }
        shapes.end()
    }

    private fun renderWater() {
        shapes.color = Color.CYAN
        shapes.begin(ShapeRenderer.ShapeType.Filled)
        for ((x, column) in waterMap.withIndex()) {
            var streakStart = 0
            var previous = WaterTiles.SAND
            for ((y, tile) in column.withIndex()) {
                val lastOne = y == column.lastIndex
                if (previous != tile || y == column.lastIndex) {
                    // streak broken
                    val color = when (previous) {
                        WaterTiles.WATER_DRAINED -> Color.SKY
                        WaterTiles.WATER_STILL -> Color.CYAN
                        else -> null
                    }
                    if (color != null) {
                        shapes.color = color
                        shapes.rect(streakStart.toFloat(), topY - x.toFloat(),
                            (y - streakStart).toFloat() + if (lastOne) 1 else 0, 1f)
                    }
                    streakStart = y
                }
                previous = tile
            }
        }
        shapes.end()
        shapes.color = Color.WHITE
        shapes.begin(ShapeRenderer.ShapeType.Line)
        for (spout in waterMap.spouts) {
            val parent = spout.parent ?: continue
            shapes.line(spout.y.toFloat(), topY - spout.x.toFloat(),
                parent.y.toFloat(), topY - parent.x.toFloat())
        }
        shapes.end()
    }


    override fun dispose() {
        shapes.dispose()
    }
}