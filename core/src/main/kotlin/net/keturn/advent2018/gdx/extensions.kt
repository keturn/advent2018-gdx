package net.keturn.advent2018.gdx

/* cycle() from Kotlin Slack. Thanks Roman Elizarov */
fun <T> Iterable<T>.cycle() = sequence { while(true) { yieldAll(this@cycle) } }

fun <K, V> Map<K, V?>.filterNotNullValues(): Map<K, V> {
    @Suppress("UNCHECKED_CAST")
    return this.filterValues { it != null } as Map<K, V>
}

fun Int.isBetween(a: Int, b: Int): Boolean {
    return if (a <= b) {
        (a <= this) && (this <= b)
    } else {
        (b <= this) && (this <= a)
    }
}

fun <E> Collection<E>.containsAny(x: Collection<E>): Boolean {
    return this.any { it in x }
}