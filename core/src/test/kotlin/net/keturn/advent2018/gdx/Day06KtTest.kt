package net.keturn.advent2018.gdx

import ch.tutteli.atrium.api.cc.en_GB.contains
import ch.tutteli.atrium.api.cc.en_GB.inAnyOrder
import ch.tutteli.atrium.api.cc.en_GB.only
import ch.tutteli.atrium.api.cc.en_GB.toBe
import ch.tutteli.atrium.api.cc.en_GB.values
import ch.tutteli.atrium.creating.Assert
import ch.tutteli.atrium.creating.AssertionPlant
import ch.tutteli.atrium.verbs.assert
import org.testng.annotations.Test


fun <E : Any, T : Iterable<E>> Assert<T>.containsOnly(expected: E, vararg otherExpected: E): AssertionPlant<T>
        = contains.inAnyOrder.only.values(expected, *otherExpected)


fun someGreatCoordinates(): List<Coordinate> {
    return listOf(
        Coordinate(203, 169),
        Coordinate(203, 296),
        Coordinate(204, 246),
        Coordinate(206, 96),
        Coordinate(207, 152),
        Coordinate(208, 235),
        Coordinate(210, 300),
        Coordinate(211, 84),
        Coordinate(213, 330),
        Coordinate(223, 319),
        Coordinate(243, 46),
        Coordinate(246, 310),
        Coordinate(247, 143),
        Coordinate(251, 160),
        Coordinate(260, 324),
        Coordinate(266, 216),
        Coordinate(271, 126),
        Coordinate(295, 304),
        Coordinate(302, 150),
        Coordinate(305, 349),
        Coordinate(312, 245),
        Coordinate(314, 195),
        Coordinate(315, 193),
        Coordinate(317, 240),
        Coordinate(319, 80),
        Coordinate(320, 348), /* kitty-corner SW, not sure if connected across tied cells? */
        Coordinate(321, 353),
        Coordinate(337, 255), /* R's north neighbor */
        Coordinate(337, 297), /* R's west neighbor */
        Coordinate(342, 79),  /* NE */
        Coordinate(351, 353),  /* R's south neighbor */
        Coordinate(354, 300)  /* Rightmost */
    )
}


class Day06KtTest {

    @Test
    fun `neighbors of R`() {
        val inputs = someGreatCoordinates()
        val thatOneCoordinate = inputs.maxBy { it.x }!!
        val neighbors = findNeighbors(inputs, thatOneCoordinate)

        assert(neighbors).containsOnly(
            Coordinate(337, 297),  /* R's west neighbor */
            Coordinate(337, 255),  /* R's north neighbor */
            Coordinate(351, 353)   /* R's south neighbor */
        )
    }

    @Test
    fun `neighbors of NE`() {
        val inputs = someGreatCoordinates()
        val coordinateNE = inputs.minBy { Coordinate(400, 0).distance(it) }!!

        val neighbors = findNeighbors(inputs, coordinateNE)

        assert(neighbors).containsOnly(Coordinate(x=319, y=80), Coordinate(x=302, y=150))
    }

    @Test
    fun `neighbors of SE`() {
        val inputs = someGreatCoordinates()
        val coordinateNE = inputs.minBy { Coordinate(400, 400).distance(it) }!!

        val neighbors = findNeighbors(inputs, coordinateNE)

        assert(neighbors).containsOnly(
            Coordinate(337, 297),  /* R's west neighbor */
            Coordinate(354, 300),  /* Rightmost */
            Coordinate(320, 348),
            Coordinate(321, 353) /* topmost */
        )
    }

    @Test
    fun `neighbors of Lime`() {
        val inputs = someGreatCoordinates()
        val limeCoordinate = inputs.single { it == Coordinate(302, 150) }

        val neighbors = findNeighbors(inputs, limeCoordinate)

        assert(neighbors).contains(Coordinate(x=342, y=79))
    }


    @Test
    fun `midway square`() {
        val a = Coordinate(10, 20)
        val b = Coordinate(12, 22)

        assert(a.xMidwayTo(b)).toBe(Coordinate(11,20))
        assert(a.yMidwayTo(b)).toBe(Coordinate(10,21))
    }

    @Test
    fun `midway tall even`() {
        val a = Coordinate(10, 20)
        val b = Coordinate(12, 26)

        assert(a.xMidwayTo(b)).toBe(Coordinate(12,21))
        assert(a.yMidwayTo(b)).toBe(Coordinate(10,23))
    }

    @Test
    fun `midway tall odd`() {
        val a = Coordinate(10, 20)
        val b = Coordinate(12, 25)

        assert(a.xMidwayTo(b)).toBe(Coordinate(12,21))
        assert(a.yMidwayTo(b)).toBe(Coordinate(10,23))
    }

    @Test
    fun `midway wide even`() {
        val a = Coordinate(10, 20)
        val b = Coordinate(16, 22)

        assert(a.xMidwayTo(b)).toBe(Coordinate(13,20))
        assert(a.yMidwayTo(b)).toBe(Coordinate(11,22))
    }

    @Test
    fun `midway wide odd`() {
        val a = Coordinate(10, 20)
        val b = Coordinate(16, 23)

        assert(a.xMidwayTo(b)).toBe(Coordinate(14,20))
        assert(a.yMidwayTo(b)).toBe(Coordinate(11,23))
    }
}

