package net.keturn.advent2018.gdx

import ch.tutteli.atrium.api.cc.en_GB.toBe
import ch.tutteli.atrium.verbs.expect
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import java.io.Serializable


private val scoringTestInputs = arrayOf(
    """
    #######
    #.G...#
    #...EG#
    #.#.#G#
    #..G#E#
    #.....#
    #######
    """.trimIndent() to Pair(47, 590),
    """
    #######
    #G..#E#
    #E#E.E#
    #G.##.#
    #...#E#
    #...E.#
    #######
    """.trimIndent() to Pair(37, 982),
    """
    #######
    #E..EG#
    #.#G.E#
    #E.##E#
    #G..#.#
    #..E#.#
    #######
    """.trimIndent() to Pair(46, 859),
    """
    #######
    #E.G#.#
    #.#G..#
    #G.#.G#
    #G..#.#
    #...E.#
    #######
    """.trimIndent() to Pair(35, 793),
    """
    #######
    #.E...#
    #.#..G#
    #.###.#
    #E#G#G#
    #...#G#
    #######
    """.trimIndent() to Pair(54, 536),
    """
    #########
    #G......#
    #.E.#...#
    #..##..G#
    #...##..#
    #...#...#
    #.G...G.#
    #.....G.#
    #########
    """.trimIndent() to Pair(20, 937),
    """
    ####
    ##E#
    #GG#
    ####
    """.trimIndent() to Pair(67, 200),
    """
    #####
    #GG##
    #.###
    #..E#
    #.#G#
    #.E##
    #####
    """.trimIndent() to Pair(71, 197)
)


class CombatTests {
    @DataProvider(name = "day 15 combats")
    fun examples(): Array<Array<Serializable>> {
        return scoringTestInputs.map { (inStr, result) -> arrayOf(inStr, result) }.toTypedArray()
    }

    @Test(dataProvider = "day 15 combats")
    fun `run full sims`(inputSpec: String, expectedResults: Pair<Int, Int>) {
        val (terrain, mobs) = combatMapFromString(inputSpec)

        val sim = CombatSimulator(terrain, mobs)
        val results = sim.runToCompletion()

        expect(results).toBe(expectedResults)
    }
}


class PathingTests {
    @Test
    fun `next-step for single target has tiebreaker of step reading order`() {
        val input = """
            #######
            #.E...#
            #.....#
            #...G.#
            #######
        """.trimIndent()
        val (terrain, mobs) = combatMapFromString(input)
        val sim = CombatSimulator(terrain, mobs)

        sim.tick()
        val elf = sim.mobs.single { it.faction == CombatFaction.ELVES }

        expect(elf.position).toBe(CombatCoordinate(3, 1))
    }

    @Test
    fun `selection from multiple targets has tiebreaker of target reading order`() {
        // https://www.reddit.com/r/adventofcode/comments/a6hldy/day_15_part_1_passing_all_tests_but_not_actual/ebvifry/
        val input = """
            #######
            #.E..G#
            #.....#
            #G....#
            #######
        """.trimIndent()
        val (terrain, mobs) = combatMapFromString(input)
        val sim = CombatSimulator(terrain, mobs)

        sim.tick()
        val elf = sim.mobs.single { it.faction == CombatFaction.ELVES }

        expect(elf.position).toBe(CombatCoordinate(3, 1))
    }

    @Test
    fun `target reading order does not depend on initial input order`() {
        // https://www.reddit.com/r/adventofcode/comments/a6hldy/day_15_part_1_passing_all_tests_but_not_actual/ebvifry/
        val input = """
            #######
            #.E..G#
            #.....#
            #G....#
            #######
        """.trimIndent()
        val (terrain, mobs) = combatMapFromString(input)
        val sim = CombatSimulator(terrain, mobs)

        val goblin1 = sim.mobAt(CombatCoordinate(5, 1))!!
        val goblin2 = sim.mobAt(CombatCoordinate(1, 3))!!

        swapPositions(goblin1, goblin2)
        sim.recomputeDistanceMaps()

        sim.tick()
        val elf = sim.mobs.single { it.faction == CombatFaction.ELVES }

        expect(elf.position).toBe(CombatCoordinate(3, 1))
    }

    @Test
    fun `target tiebreaker according to enemy-adjacent coordinate, not enemy coordinate`() {
        val input = """
            ##########
            ##G#.....#
            ##.#E...G#
            #........#
            ##G#######
            ##########
        """.trimIndent()  // NOTE top goblin will move down one before elf move

        val (terrain, mobs) = combatMapFromString(input)
        val sim = CombatSimulator(terrain, mobs)

        sim.tick()
        val elf = sim.mobs.single { it.faction == CombatFaction.ELVES }

        expect(elf.position).toBe(CombatCoordinate(5, 2))
    }
}


fun swapPositions(mob1: Mob, mob2: Mob) {
    val tmp = mob1.position
    mob1.position = mob2.position
    mob2.position = tmp
}