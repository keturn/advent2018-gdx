package net.keturn.advent2018.gdx

import ch.tutteli.atrium.api.cc.en_GB.toBe
import ch.tutteli.atrium.verbs.expect
import org.testng.annotations.Test


private val exampleInput = """
            x=495, y=2..7
            y=7, x=495..501
            x=501, y=3..7
            x=498, y=2..4
            x=506, y=1..2
            x=498, y=10..13
            x=504, y=10..13
            y=13, x=498..504
        """.trimIndent()


class Day17Test {
    @Test
    fun `parse and render example input`() {
        val expected = """
            ..............
            ............#.
            .#..#.......#.
            .#..#..#......
            .#..#..#......
            .#.....#......
            .#.....#......
            .#######......
            ..............
            ..............
            ....#.....#...
            ....#.....#...
            ....#.....#...
            ....#######...
        """.trimIndent()
        val expectedMinX = 494

        val clayRanges = inputClay(exampleInput.lineSequence())
        val waterMap = WaterMap(clayRanges)

        verifyWaterMapLooksLike(waterMap.array, expected, expectedMinX)
    }


    @Test
    fun `test flow on example input`() {
        val expected = """
            ......|.......
            ......|.....#.
            .#..#||||...#.
            .#..#~~#|.....
            .#..#~~#|.....
            .#~~~~~#|.....
            .#~~~~~#|.....
            .#######|.....
            ........|.....
            ...|||||||||..
            ...|#~~~~~#|..
            ...|#~~~~~#|..
            ...|#~~~~~#|..
            ...|#######|..
        """.trimIndent()

        val expectedMinX = 494

        val clayRanges = inputClay(exampleInput.lineSequence())
        val waterMap = WaterMap(clayRanges)

        waterMap.runUntilCompletion()

        verifyWaterMapLooksLike(waterMap.array, expected, expectedMinX)
    }

    private fun verifyWaterMapLooksLike(array: WaterScanArray, s: String, minX: Int = 0) {
        val lines = s.lines()
        for ((y, line) in lines.withIndex()) {
            val resultLine = array.sliceRow(y, minX, line.count())
            expect(resultLine.toMapString()).toBe(line)
        }
    }

    @Test
    fun `test empty map is immediately drained`() {
        val waterMap = WaterMap(WaterScanArray(11, 5), 5)

        val expected = """
            .....|.....
            .....|.....
            .....|.....
            .....|.....
            .....|.....
        """.trimIndent()

        waterMap.runUntilCompletion()

        verifyWaterMapLooksLike(waterMap, expected)
    }

    @Test
    fun `water runs off horizontal obstruction`() {
        val input = """
            .....{.....
            ......#....
            ....###....
            ...........
        """.trimIndent()
        val expected = """
            .....|.....
            ...|||#....
            ...|###....
            ...|.......
        """.trimIndent()

        val waterMap = WaterMap(input)
        waterMap.runUntilCompletion()

        verifyWaterMapLooksLike(waterMap, expected)
    }

    @Test
    fun `if half a split drains, it all drains`() {
        val input = """
            .....{.....
            ........#..
            ....#####..
            ...........
        """.trimIndent()
        val expected = """
            .....|.....
            ...|||||#..
            ...|#####..
            ...|.......
        """.trimIndent()

        val waterMap = WaterMap(input)
        waterMap.runUntilCompletion()
        verifyWaterMapLooksLike(waterMap, expected)
    }

    @Test
    fun `filling a basin`() {
        val input = """
            .....{.....
            ......#....
            ...#..#....
            ...####....
            """.trimIndent()
        val expected = """
            .....|.....
            ..||||#....
            ..|#~~#....
            ..|####....
            """.trimIndent()

        val waterMap = WaterMap(input)
        waterMap.runUntilCompletion()
        verifyWaterMapLooksLike(waterMap, expected)
    }


    @Test
    fun `split basin`() {
        val input = """
            .....{.....
            ...........
            .....#.....
            .....#.....
            .....#..#..
            ..#.....#..
            ..#######..
            """.trimIndent()
        val expected = """
            .....|.....
            ....|||....
            ....|#|....
            ....|#||||.
            .||||#~~#|.
            .|#~~~~~#|.
            .|#######|.
            """.trimIndent()

        val waterMap = WaterMap(input)
        waterMap.runUntilCompletion()

        verifyWaterMapLooksLike(waterMap, expected)
    }
}


@Suppress("unused")
private fun WaterMap.print() {
    for (y in 0..this.maxY) {
        val row = this.sliceRow(y, 0, this.xSize)
        println(row.toMapString())
    }
}


@Suppress("TestFunctionName")
private fun WaterMap(s: String): WaterMap {
    val lines = s.lines()
    val width = lines[0].count()
    val height = lines.count()

    var springX = SPRING_X
    val array = WaterScanArray(width, height)
    for ((y, row) in lines.withIndex()) {
        for ((x, c) in row.withIndex()) {
            if (c == '{') {
                springX = x
            }
            array[x, y] = if (c == '#') WaterTiles.CLAY else WaterTiles.SAND
        }
    }
    return WaterMap(array, springX)
}


internal fun WaterScanArray.sliceRow(y: Int, minX: Int, count: Int): List<WaterTiles> {
    return (minX until (minX+count)).map { x -> this[x][y] }
}

internal fun List<WaterTiles>.toMapString(): String {
    return map {
        when(it) {
            WaterTiles.SAND -> '.'
            WaterTiles.CLAY -> '#'
            WaterTiles.WATER_STILL -> '~'
            WaterTiles.WATER_DRAINED -> '|'
        }
    }.joinToString("")
}
