@file:JvmName("DesktopLauncher")

package net.keturn.advent2018.gdx.desktop

import com.badlogic.gdx.Files
import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration

import net.keturn.advent2018.gdx.Advent2018

/** Launches the desktop (LWJGL) application. */
fun main(args: Array<String>) {
    LwjglApplication(Advent2018(), LwjglApplicationConfiguration().apply {
        title = "advent2018-gdx"
        width = 1280
        height = 900
        resizable = true
        intArrayOf(128, 64, 32, 16).forEach{
            addIcon("libgdx$it.png", Files.FileType.Internal)
        }
    })
}
